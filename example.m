% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
%         SYNTHETIC DATA FOR STRAIN INVERSION
%
% % % % % % % % % % % % % % % % % % % % % % % % % % % %

clear all

import unicycle.*

load '../gmt/boundaries.dat';

G=30e3;
nu=1/4;

flatten=@(x) x(:);

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%           R E C E I V E R   F A U L T S              %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

flt=unicycle.geometry.triangleReceiver('../faults/qiu+15_1_receiver',unicycle.greens.nikkhoo15(G,nu));

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%              S O U R C E   F A U L T S               %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

src=[];%geometry.source();

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%           C O S E I S M I C   E V E N T S            %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

evt={unicycle.geometry.coseismicTriangle('../faults/qiu+15_1',0,unicycle.greens.nikkhoo15(G,nu))};

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%      R E C E I V E R   S H E A R   Z O N E S         %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

phn=unicycle.geometry.polyhedronReceiver('../faults/ramp',unicycle.greens.polyhedronShearZone18(1,0.25));

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                 G P S   d a t a                      %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

gps=unicycle.manifold.gpsReceiver([4e4*flatten(repmat((1:12),10,1))-2.0e5,4e4*flatten(repmat((1:10),12,1)')-2e5,zeros(12*10,1)],[],3);

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%            G E O M E T R Y   C H E C K               %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

if true
    figure(1000);clf;set(gcf,'name','Geometry & properties');
    axis equal, box on, hold on
    
    % plot volume mesh
    phn.plotVolume();
    
    % plot fault mesh
    flt.plotPatch();
    
    % political boundaries
    plot(boundaries(:,1),boundaries(:,2),'k-','lineWidth',1)
    
    % GPS network
    plot3(gps.x(:,1),gps.x(:,2),gps.x(:,3),'k^','MarkerFaceColor','k','MarkerSize',10);
    
    box on, grid on;
    xlabel('east (m)');ylabel('north (m)'); zlabel('up (m)');
    axis equal
    set(gca,'xlim',[-180 280]*1e3,'ylim',[-180 180]*1e3)
end


%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%              S T R E S S   K E R N E L S             %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%


% build stress kernels for integral equation
tic
fprintf('# compute stress and traction kernels\n')
evl=ode.maxwell([],[],phn,evt,'./kernels/');
toc

tic
fprintf('# compute displacement kernels\n')
gps.displacementKernels(evl);
toc

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
%            S Y N T H E T I C   D A T A
%
%% % % % % % % % % % % % % % % % % % % % % % % % % % % %

% target model
eta=1e18/1e6/(60*60*24*365); % viscosity (MPa yr)
phn.e11=evl.evt{1}.KL{1,1}*(evt{1}.slip.*cosd(evt{1}.rake))+evl.evt{1}.KL{2,1}*(evt{1}.slip.*sind(evt{1}.rake));
phn.e12=evl.evt{1}.KL{1,2}*(evt{1}.slip.*cosd(evt{1}.rake))+evl.evt{1}.KL{2,2}*(evt{1}.slip.*sind(evt{1}.rake));
phn.e13=evl.evt{1}.KL{1,3}*(evt{1}.slip.*cosd(evt{1}.rake))+evl.evt{1}.KL{2,3}*(evt{1}.slip.*sind(evt{1}.rake));
phn.e22=evl.evt{1}.KL{1,4}*(evt{1}.slip.*cosd(evt{1}.rake))+evl.evt{1}.KL{2,4}*(evt{1}.slip.*sind(evt{1}.rake));
phn.e23=evl.evt{1}.KL{1,5}*(evt{1}.slip.*cosd(evt{1}.rake))+evl.evt{1}.KL{2,5}*(evt{1}.slip.*sind(evt{1}.rake));
phn.e33=evl.evt{1}.KL{1,6}*(evt{1}.slip.*cosd(evt{1}.rake))+evl.evt{1}.KL{2,6}*(evt{1}.slip.*sind(evt{1}.rake));

% remove isotropic component
ekk=phn.e11+phn.e22+phn.e33;
phn.e11=phn.e11-ekk/3;
phn.e22=phn.e22-ekk/3;
phn.e33=phn.e33-ekk/3;

% synthetic data
gps.d=gps.LO{1}*phn.e11 ...
     +gps.LO{2}*phn.e12 ...
     +gps.LO{3}*phn.e13 ...
     +gps.LO{4}*phn.e22 ...
     +gps.LO{5}*phn.e23 ...
     +gps.LO{6}*phn.e33;

% target model
mt=[phn.e11;phn.e12;phn.e13;phn.e22;phn.e23;phn.e33];

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%          S T R E S S   I N T E R A C T I O N         %
%               V I S U A L I Z A T I O N              %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

if true
    figure(1001);clf;set(gcf,'name','Target model');
    hold on
    
    toplot=phn.e11;
    phn.plotVolume(toplot);
    
    % fault slip
    %toplot=evt{1}.slip;
    %evt{1}.plotPatch(toplot);
    %evt{1}.plotPatch();
    
    % political boundaries
    plot(boundaries(:,1),boundaries(:,2),'k-','lineWidth',2)
    
    % GPS stations
    sc=1e1;
    plot3(gps.x(:,1),gps.x(:,2),gps.x(:,3),'k^','MarkerFaceColor','k','MarkerSize',10);
    quiver3(gps.x(:,1),gps.x(:,2),gps.x(:,3), ...
        sc*gps.d(1:3:end),sc*gps.d(2:3:end),sc*gps.d(3:3:end),0);
    
    colormap(jet);
    h=colorbar();
    ylabel(h,'Strain-rate (1/s)');
    box on, grid on;
    xlabel('East (m)');ylabel('North (m)'); zlabel('Up (m)');
    %set(gca,'view',[-227 42]);
    set(gca,'clim',[-1 1]*max(abs(get(gca,'clim')))/1e0);
    axis equal
    set(gca,'xlim',[-150 300]*1e3,'ylim',[-200 200]*1e3);
    fprintf('Stress interaction: review before simulation\n');
end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                C O N S T R A I N T S                 %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

Q={zeros(phn.N,phn.N*6),zeros(phn.N,phn.N*6),zeros(phn.N,phn.N*6),zeros(phn.N,phn.N*6),zeros(phn.N,phn.N*6)};
for k=1:numel(phn.e11)
    sigma=[phn.e11(k) phn.e12(k) phn.e13(k) phn.e22(k) phn.e23(k) phn.e33(k)]';
    [U,~,~]=svd(sigma);
    for j=2:6
        Q{j-1}(k,k:phn.N:end)=U(:,j)';
    end
end
D=zeros(phn.N,phn.N*6);
for k=1:numel(phn.e11)
    D(k,k+phn.N*[0,3,5])=ones(3,1); %zeros(3,1);
end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                  I N V E R S I O N                   %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

gps.d = d;

G=[gps.LO{1},gps.LO{2},gps.LO{3},gps.LO{4},gps.LO{5},gps.LO{6}];

L=[ evl.LL{1,1},evl.LL{2,1},evl.LL{3,1},evl.LL{4,1},evl.LL{5,1},evl.LL{6,1}; ...
    evl.LL{1,2},evl.LL{2,2},evl.LL{3,2},evl.LL{4,2},evl.LL{5,2},evl.LL{6,2}; ...
    evl.LL{1,3},evl.LL{2,3},evl.LL{3,3},evl.LL{4,3},evl.LL{5,3},evl.LL{6,3}; ...
    evl.LL{1,4},evl.LL{2,4},evl.LL{3,4},evl.LL{4,4},evl.LL{5,4},evl.LL{6,4}; ...
    evl.LL{1,5},evl.LL{2,5},evl.LL{3,5},evl.LL{4,5},evl.LL{5,5},evl.LL{6,5}; ...
    evl.LL{1,6},evl.LL{2,6},evl.LL{3,6},evl.LL{4,6},evl.LL{5,6},evl.LL{6,6}];

H=[G;1e0*L];
h=[gps.d;zeros(size(L,1),1)];

P=[G;    0e0*L;             1e1*Q{1};1e1*Q{2};1e1*Q{3};1e1*Q{4};1e1*Q{5};1e1*D];
p=[gps.d;zeros(size(L,1),1);zeros(size(Q{1},1)*5,1);zeros(size(D,1),1)];

fprintf('# model 1, least-squares by QR decomposition, fewest possible non-zero components\n');
m1=G\gps.d;
rho=1-sum((m1-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m1).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 2, least-squares inversion by QR decomposition with smoothing, fewest possible non-zero components\n');
m2=H\h;
rho=1-sum((m2-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m2).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 3, least-squares inversion, no smoothing\n');
m3=(G.'*G)\(G.'*gps.d);
rho=1-sum((m3-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m3).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 4, least-squares inversion, smoothing\n');
m4=(H.'*H)\(H.'*h);
rho=1-sum((m4-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m4).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 5, pseudo-inverse, no smoothing\n');
tolerance=1e-1;
m5=pinv(G,tolerance)*gps.d;
rho=1-sum((m5-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m5).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 6, pseudo-inverse, smoothing\n');
tolerance=1e-1;
m6=pinv(H'*H,tolerance)*(H'*h);
rho=1-sum((m6-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m6).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 7, pseudo-inverse with smoothing\n');
tolerance=1e-1;
m7=pinv(H,tolerance)*h;
rho=1-sum((m7-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m7).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 8, sparse Basis pursuit denoising solution\n');
sigma=0.09; % ||Ax - b||_2 < sigma
m8=spgl1.bpdn(G,gps.d,sigma);
rho=1-sum((m8-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m8).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 9, sparse Lasso solution\n');
tau=60; % || m ||_1 < tau
m9=spgl1.lasso(G,gps.d,tau);
rho=1-sum((m9-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m9).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 10, LS by QR decomposition with smoothing and constraints\n');
m10=P\p;
rho=1-sum((m10-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m10).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 11, LS, smoothing and constraints\n');
m11=(P.'*P)\(P.'*p);
rho=1-sum((m11-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m11).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 12, pseudo-inverse, smoothing and constraints\n');
tolerance=1e-1;
m12=pinv(P'*P,tolerance)*(P'*p);
rho=1-sum((m12-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m12).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

fprintf('# model 13, pseudo-inverse with smoothing and constraints\n');
tolerance=1e-1;
m13=pinv(P,tolerance)*p;
rho=1-sum((m13-mt).^2)/sum(mt.^2);
theta=1-sum((gps.d-G*m13).^2)/sum(gps.d.^2);
fprintf('# variance reduction: %f%%, model similarity: %f%%\n\n',theta*100,rho*100);

%% OutIBI

QQ = [Q{1};Q{2};Q{3};Q{4};Q{5}];

% specify inequality constraint Am > a
M = size(G,2);
A = [speye(M);-speye(M)];
a = -10* max(abs(mt)) * ones(2*M,1);

% generate data with noise
d = gps.d + randn(length(gps.d),1);

[m_samples,lambda_samples,delta_samples,gamma_samples]=OutIBI({G},{speye(length(d))},{d},{L,sparse(QQ),sparse(D)},{sparse(size(L,1),1),sparse(size(QQ,1),1),sparse(size(D,1),1)},ones(4,1),A,a,2e5);

% compute mean, median, and mode
nburnin = 1.4e5;
m_mean = mean(m_samples(:,nburnin+1:end),2);
m_median = median(m_samples(:,nburnin+1:end),2);
m_mode = annealedmeanshift(m_median,m_samples(:,nburnin+1:end),10);

delta_mean = mean(delta_samples{1}(:,nburnin+1:end),2);
delta_median = median(delta_samples{1}(:,nburnin+1:end),2);
delta_mode = annealedmeanshift(delta_median,delta_samples{1}(:,nburnin+1:end),10);
%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%      P L O T   I N V E R S I O N   R E S U L T S     %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% d = gps.d;
% model selection

m = m_mean;
delta = delta_mean;

% model predictions
dt=G*m+delta;

% residuals between observations and best-fitting model
r=d-dt;

% variance reduction
theta=1-sum(r.^2)/sum(d.^2);

% model similarity
rho=1-sum((m-mt).^2)/sum(mt.^2);

fprintf('# variance reduction %f%%, model similarity: %f%%\n',theta*100,rho*100);

% model selection
model.e11=m(0*phn.N+1:1*phn.N);
model.e12=m(1*phn.N+1:2*phn.N);
model.e13=m(2*phn.N+1:3*phn.N);
model.e22=m(3*phn.N+1:4*phn.N);
model.e23=m(4*phn.N+1:5*phn.N);
model.e33=m(5*phn.N+1:6*phn.N);

if true
    figure(1009);clf;set(gcf,'name','Inversion results');
    
    subplot(1,2,1);cla;hold on;grid on;axis equal;
    
    % strain distribution
    toplot=sqrt(phn.e11.^2+2*phn.e12.^2+2*phn.e13.^2+phn.e22.^2+2*phn.e23.^2+phn.e33.^2);
    %toplot=phn.e33;
    phn.plotVolume(toplot);
    
    % GPS stations
    sc=1e1;
    plot3(gps.x(:,1),gps.x(:,2),gps.x(:,3),'k^','MarkerFaceColor','k','MarkerSize',10);
    quiver3(gps.x(:,1),gps.x(:,2),gps.x(:,3), ...
        sc*d(1:3:end),sc*d(2:3:end),sc*d(3:3:end),0);
    
    % political boundaries
    plot(boundaries(:,1),boundaries(:,2),'k-','lineWidth',2)
    
    colormap(jet);
    h=colorbar();
    ylabel(h,'Strain-rate (1/s)');
    box on, grid on;
    xlabel('East (m)');ylabel('North (m)'); zlabel('Up (m)');
    set(gca,'clim',[-1.6,1.6]); %[-1 1]*max(abs(get(gca,'clim')))/1e0);
    set(gca,'xlim',[-150 300]*1e3,'ylim',[-200 200]*1e3);
    title('Target model');
    
    
    subplot(1,2,2);cla;hold on;grid on;axis equal;
    
    % strain distribution
    toplot=sqrt(model.e11.^2+2*model.e12.^2+2*model.e13.^2+model.e22.^2+2*model.e23.^2+model.e33.^2);
    %toplot=model.e33;
    phn.plotVolume(toplot);
    
    % GPS stations
    sc=1e1;
    plot3(gps.x(:,1),gps.x(:,2),gps.x(:,3),'k^','MarkerFaceColor','k','MarkerSize',10);
    quiver3(gps.x(:,1),gps.x(:,2),gps.x(:,3), ...
        sc*dt(1:3:end),sc*dt(2:3:end),sc*dt(3:3:end),0);
    
    % political boundaries
    plot(boundaries(:,1),boundaries(:,2),'k-','lineWidth',2)
    
    colormap(jet);
    h=colorbar();
    ylabel(h,'Strain-rate (1/s)');
    box on, grid on;
    xlabel('East (m)');ylabel('North (m)'); zlabel('Up (m)');
    set(gca,'clim',[-1.6,1.6]); %[-1 1]*max(abs(get(gca,'clim')))/1e0);
    set(gca,'xlim',[-150 300]*1e3,'ylim',[-200 200]*1e3);
    title('Inverted model');
    
end
%%
m025 = quantile(m_samples(:,nburnin+1:end),0.025,2);
m975 = quantile(m_samples(:,nburnin+1:end),0.975,2);
figure; fill([1:1728,1728:-1:1],[m975;flip(m025)]','c','EdgeColor','none');
hold on; plot(mt,'r');
hold on; plot(m,'b--');
hold on; plot(m025,'b-.');
hold on; plot(m975,'b-.');
xlabel('model parameters');
ylabel('value');
legend('95% condidence interval','true model parameters','esimated model parameters');
ylim([-8,8]);