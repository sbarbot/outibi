function [z, lower_bound, upper_bound] = truncMVN_rankdeficient(z, mu_z, std_z, id_nonzero, id_zero, ...
    A_tilde, a, M, randn_vec, randu_vec)

for i = id_nonzero
    ic = setdiff(1:M,i);
    idp = find(A_tilde(:,i)<0);
    %             bounds = (a-A(:,ic)*m(ic))./abs(A(:,i));
    if ~isempty(idp)
        upper_bound(i) = min((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        upper_bound(i) = Inf;
    end
    idp = find(A_tilde(:,i)>0);
    if ~isempty(idp)
        lower_bound(i) = max((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        lower_bound(i) = -Inf;
    end
    if lower_bound(i) - upper_bound(i) > 1e-3
       fprintf('lower bound - upper bound = %f\n', lower_bound(i) - upper_bound(i));
       error('Error: the inequality constraints do not define a feasible set.');
    end
    cdf_upper_bound = normcdf(upper_bound(i),mu_z(i),std_z(i));
    cdf_lower_bound = normcdf(lower_bound(i),mu_z(i),std_z(i));
    if cdf_lower_bound > 1 - 1e-8 || cdf_upper_bound < 1e-8
        if upper_bound(i) == Inf %isempty(upper_bound(i))
            z(i) = lower_bound(i) + std_z(i)*abs(randn_vec(i)); %*1e-4;
        elseif lower_bound(i) == -Inf %isempty(lower_bound(i))
            z(i) = upper_bound(i) - std_z(i)*abs(randn_vec(i)); %*1e-4;
        else
            z(i) = (upper_bound(i)-lower_bound(i))*randu_vec(i)+lower_bound(i);
        end
    else
        z(i) = norminv((cdf_upper_bound-cdf_lower_bound)*randu_vec(i)+cdf_lower_bound,mu_z(i),std_z(i));
    end
end
for i = id_zero
    ic = setdiff(1:M,i);
    idp = find(A_tilde(:,i)<0);
    if ~isempty(idp)
        upper_bound(i) = min((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        upper_bound(i) = [];
    end
    idp = find(A_tilde(:,i)>0);
    if ~isempty(idp)
        lower_bound(i) = max((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        lower_bound(i) = [];
    end
    if isempty(lower_bound(i)) || isempty(upper_bound(i))
        error('Error: The inverse covariance matrix J is rank deficient or close to singular,' + ...
            'please provide more data or equality or inequality constraints!')
    else
        z(i) = (upper_bound(i)-lower_bound(i))*randu_vec(i)+lower_bound(i);
    end
end