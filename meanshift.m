function xh = meanshift(x0,m,sig)

[P,K] = size(m);
diff = zeros(K,1);

vinv = sig.^(-2);
while 1
    for i = 1:K
        diff(i) = sum((x0-m(:,i)).^2);
    end
    fx = -P*log(sig) - diff.*vinv/2;   %sum((repmat(x0,1,K)-m).^2);
    fx = fx - max(fx);
    fx = exp(fx);
    xh = m*(fx.*vinv)/sum(fx.*vinv);
    if mean(abs(xh-x0)) < 1e-4
        break;
    else
        x0 = xh;
    end
end





