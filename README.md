# OutIBI

Outlier-Insensitive Bayesian Inference for Linear Inverse Problems (OutIBI) implements the Gibbs sampling algorithm for linear inverse problems with outliers.

Reference:

Hang Yu, Sylvain Barbot, Justin Dauwels, Teng Wang, Priyamvada Nanjundiah, and Qiang Qiu, Outlier-Insensitive Bayesian Inference for Linear Inverse Problems (OutIBI) with Applications to Space Geodetic Data, Geophysical Journal International, in press, 2020.