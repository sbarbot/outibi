The C++ code is written based on the template-based C++ library Armadillo. 
To achieve the best performance, it is better to link the C++ code with
BLAD and LAPACK in Intel MKL.

The mex code for Windows and Ubuntu OS have been provided. 

In the case the mex code is obsolete, you may compile the original C++ code 
into the mex code following the instructions below.

Before mexing the C++ code, please download and install Intek MKL from
https://software.intel.com/en-us/mkl

In particular for Ubuntu OS, Intel MLK can be downloaded and installed by 
running the following commands in the terminal:

cd /tmp
sudo wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB
sudo apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB
sudo sh -c 'echo deb https://apt.repos.intel.com/mkl all main > /etc/apt/sources.list.d/intel-mkl.list'
sudo apt-get update
sudo apt-get install intel-mkl-64bit-2018.2-046


To mex the C++ code in Windows using the msvc compiler, please run

win_msvc_IntelMKL_mex.m

To mex the C++ code in Ubuntu using the g++ compiler, please run

ubuntu_gpp_IntelMKL_mex.m

We also provide the matlab code for the two functions in the corresponding 
m files. MATLAB would choose to run the m code once the mex files are deleted. 
Note that the m code is slower than the mex code.

You may need to change directory of Intel MKL to your own installation 
directory in the above m files.