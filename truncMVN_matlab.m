function z = truncMVN_matlab(z, mu_z, std_z, id_nonzero, id_zero, ...
    A_tilde, a, M, randn_vec, randu_vec)

for i = id_nonzero
    ic = setdiff(1:M,i);
    idp = find(A_tilde(:,i)<0);
    %             bounds = (a-A(:,ic)*m(ic))./abs(A(:,i));
    if ~isempty(idp)
        upper_bound = min((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        upper_bound = Inf;
    end
    idp = find(A_tilde(:,i)>0);
    if ~isempty(idp)
        lower_bound = max((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        lower_bound = -Inf;
    end
    if lower_bound > upper_bound
        lower_bound - upper_bound
    end
    cdf_upper_bound = normcdf(upper_bound,mu_z(i),std_z(i));
    cdf_lower_bound = normcdf(lower_bound,mu_z(i),std_z(i));
    z(i) = norminv((cdf_upper_bound-cdf_lower_bound)*randu_vec(i)+cdf_lower_bound,mu_z(i),std_z(i));
    if isinf(z(i))
        if upper_bound == Inf %isempty(upper_bound)
            z(i) = lower_bound + std_z(i)*abs(randn_vec(i)); %*1e-4;
        elseif lower_bound == -Inf %isempty(lower_bound)
            z(i) = upper_bound - std_z(i)*abs(randn_vec(i)); %*1e-4;
        else
            z(i) = (upper_bound-lower_bound)*randu_vec(i)+lower_bound;
        end
    end
end
for i = id_zero
    ic = setdiff(1:M,i);
    idp = find(A_tilde(:,i)<0);
    if ~isempty(idp)
        upper_bound = min((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        upper_bound = [];
    end
    idp = find(A_tilde(:,i)>0);
    if ~isempty(idp)
        lower_bound = max((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        lower_bound = [];
    end
    if isempty(lower_bound) || isempty(upper_bound)
        error('Error: The inverse covariance matrix J is rank deficient or close to singular,' + ...
            'please provide more data or equality or inequality constraints!')
    else
        z(i) = (upper_bound-lower_bound)*randu_vec(i)+lower_bound;
    end
end