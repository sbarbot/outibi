#include "armaMex.hpp"
#include <boost/math/special_functions/erf.hpp>
//#include <boost/math/special_functions/fpclassify.hpp>
        
double norminv(double cdf, double mu, double std) {
    double z = mu - std * sqrt(2) * boost::math::erfc_inv(2 * cdf);
    return z;
}

void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    // load inputs
    
    vec z = armaGetPr(prhs[0]);
    vec mu_z = armaGetPr(prhs[1]);
    vec std_z = armaGetPr(prhs[2]);
    uvec id_nonzero = conv_to<uvec>::from(armaGetPr(prhs[3])) - 1;  
    // the indices of C++ starts from 0
    uvec id_zero = conv_to<uvec>::from(armaGetPr(prhs[4])) - 1;
    mat A_tilde = armaGetPr(prhs[5]);
    vec a = armaGetPr(prhs[6]);
    uword M = (uword)armaGetDouble(prhs[7]);
    vec randn_vec = armaGetPr(prhs[8]);
    vec randu_vec = armaGetPr(prhs[9]);
    
    vec lb(M, fill::zeros), ub(M, fill::zeros);
    
    // declare and initialize variables
    uword i, idi_scalar, M_minus_1 = M - 1, M_minus_2 = M - 2;
    uvec idi(1), ic(M - 1), idp, id_all = linspace<uvec>(0, M_minus_1, M);
    double upper_bound, lower_bound, cdf_upper_bound, cdf_lower_bound, z_idi;
    bool no_upper_bound = false, no_lower_bound = false;
    // Gibbs sampling from truncated Gaussian with diagonal covariance
    for (i = 0; i < id_nonzero.n_elem; i++) {
        idi_scalar = id_nonzero(i);
        idi.fill(idi_scalar);
        ic = join_cols(id_all.head(idi_scalar), id_all.tail(M_minus_1 - idi_scalar));
        idp = find(A_tilde.col(idi_scalar) < 0);
        if (!idp.is_empty()) {
            upper_bound = min((a.elem(idp) - A_tilde.submat(idp, ic) * 
                    z.elem(ic)) / A_tilde.submat(idp, idi));
            idp.clear();
            cdf_upper_bound = normcdf(upper_bound, mu_z(idi_scalar), 
                    std_z(idi_scalar));
            no_upper_bound = false;
        } else {
            cdf_upper_bound = 1;
            no_upper_bound = true;
        }
        idp = find(A_tilde.col(idi_scalar) > 0);
        if (!idp.is_empty()) {
            lower_bound = max((a.elem(idp) - A_tilde.submat(idp, ic) * 
                    z.elem(ic)) / A_tilde.submat(idp, idi));
            idp.clear();
            cdf_lower_bound = normcdf(lower_bound, mu_z(idi_scalar), 
                    std_z(idi_scalar));
            no_lower_bound = false;
        } else {
            cdf_lower_bound = 0;
            no_lower_bound = true;
        }
        if (cdf_lower_bound - cdf_upper_bound > 1e-3) {
            mexPrintf("lower bound - upper bound = %f\n", lower_bound - upper_bound);
            mexErrMsgTxt("Error: the inequality constraints do not define a feasible set"); 
        }
        if ((cdf_lower_bound > 0.99999999) | (cdf_upper_bound < 1e-8)) {
            if (no_upper_bound) 
                z(idi_scalar) = lower_bound + std_z(idi_scalar) * abs(randn_vec(idi_scalar));
            else if (no_lower_bound)
                z(idi_scalar) = upper_bound - std_z(idi_scalar) * abs(randn_vec(idi_scalar));
            else
                z(idi_scalar) = (upper_bound-lower_bound)*randu_vec(idi_scalar)+lower_bound;
        } else 
            z(idi_scalar) = norminv((cdf_upper_bound - cdf_lower_bound) * randu_vec(idi_scalar) 
                + cdf_lower_bound, mu_z(idi_scalar), std_z(idi_scalar));
    }
    
    no_upper_bound = false;
    no_lower_bound = false;
    
    for (i = 0; i < id_zero.n_elem; i++) {
        idi_scalar = id_zero(i);
        idi.fill(idi_scalar);
        ic = join_cols(id_all.head(idi_scalar), id_all.tail(M_minus_1 - idi_scalar));
        idp = find(A_tilde.col(idi_scalar) < 0);
        if (!idp.is_empty()) {
            upper_bound = min((a.elem(idp) - A_tilde.submat(idp, ic) * 
                    z.elem(ic)) / A_tilde.submat(idp, idi));
            idp.clear();
        } else no_upper_bound = true;
        idp = find(A_tilde.col(idi_scalar) > 0);
        if (!idp.is_empty()) {
            lower_bound = max((a.elem(idp) - A_tilde.submat(idp, ic) * 
                    z.elem(ic)) / A_tilde.submat(idp, idi));
            idp.clear();
        } else no_lower_bound = true;
        if (no_lower_bound || no_upper_bound)
            mexErrMsgTxt("Error: The inverse covariance matrix J is rank\
                    deficient or close to singular, please provide more data\
                    or equality or inequality constraints!");
        else {
            if (lower_bound - upper_bound > 1e-3) {
                mexPrintf("lower bound - upper bound = %f\n", lower_bound - upper_bound);
                mexErrMsgTxt("Error: the inequality constraints do not define\
                        a feasible set");
            }
            z(idi_scalar) = (upper_bound-lower_bound)*randu_vec(idi_scalar)+lower_bound;
            ub(idi_scalar) = upper_bound;
            lb(idi_scalar) = lower_bound;
        }
            
    }
    
    // outputs
    plhs[0] = armaCreateMxMatrix(M, 1, mxDOUBLE_CLASS,mxREAL);
    armaSetPr(plhs[0],z);
    plhs[1] = armaCreateMxMatrix(M, 1, mxDOUBLE_CLASS,mxREAL);
    armaSetPr(plhs[1],lb);
    plhs[2] = armaCreateMxMatrix(M, 1, mxDOUBLE_CLASS,mxREAL);
    armaSetPr(plhs[2],ub);
}