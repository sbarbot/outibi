#include "armaMex.hpp"
#include <boost/math/special_functions/erf.hpp>
//#include <boost/math/special_functions/fpclassify.hpp>
        
double norminv(double cdf, double mu) {
    double z = mu - sqrt(2) * boost::math::erfc_inv(2 * cdf);
    return z;
}

void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    // load inputs
    
    vec z = armaGetPr(prhs[0]);
    vec mu_z = armaGetPr(prhs[1]);
    mat A_tilde = armaGetPr(prhs[2]);
    vec a = armaGetPr(prhs[3]);
    uword M = (uword)armaGetDouble(prhs[4]);
    vec randn_vec = armaGetPr(prhs[5]);
    vec randu_vec = armaGetPr(prhs[6]);
    
    // declare and initialize variables
    uword i, M_minus_1 = M - 1, count_bd = 0;
    uvec idi(1), ic(M - 1), idp, id_all = linspace<uvec>(0, M_minus_1, M);
    double upper_bound, lower_bound, cdf_upper_bound, cdf_lower_bound, z_idi;
    bool no_upper_bound = false, no_lower_bound = false;
    // Gibbs sampling from truncated Gaussian with diagonal covariance
    for (i = 0; i < M; i++) {
        idi.fill(i);
        ic = join_cols(id_all.head(i), id_all.tail(M_minus_1 - i));
        idp = find(A_tilde.col(i) < 0);
        if (!idp.is_empty()) {
            upper_bound = min((a.elem(idp) - A_tilde.submat(idp, ic) * 
                    z.elem(ic)) / A_tilde.submat(idp, idi));
            idp.clear();
            cdf_upper_bound = normcdf(upper_bound - mu_z(i));
            no_upper_bound = false;
        } else {
            cdf_upper_bound = 1;
            no_upper_bound = true;
        }
        idp = find(A_tilde.col(i) > 0);
        if (!idp.is_empty()) {
            lower_bound = max((a.elem(idp) - A_tilde.submat(idp, ic) * 
                    z.elem(ic)) / A_tilde.submat(idp, idi));
            idp.clear();
            cdf_lower_bound = normcdf(lower_bound - mu_z(i));
            no_lower_bound = false;
        } else {
            cdf_lower_bound = 0;
            no_lower_bound = true;
        }
        if (cdf_lower_bound - cdf_upper_bound > 1e-3) {
            mexPrintf("lower bound - upper bound = %f\n", lower_bound - upper_bound);
            mexErrMsgTxt("Error: the inequality constraints do not define a feasible set"); 
        }
        if ((cdf_lower_bound > 0.99999999) || (cdf_upper_bound < 1e-8)) {
            if (no_upper_bound) 
                z(i) = lower_bound + abs(randn_vec(i));
            else if (no_lower_bound)
                z(i) = upper_bound - abs(randn_vec(i));
            else
                z(i) = (upper_bound - lower_bound) * randu_vec(i)+lower_bound;
        } else { 
            z(i) = norminv((cdf_upper_bound - cdf_lower_bound) * randu_vec(i) 
                + cdf_lower_bound, mu_z(i));
        }
        if ((cdf_lower_bound < 1e-8) && (cdf_upper_bound > 0.99999999))
            count_bd ++;
    }
    
    
    // outputs to MATLAB
    plhs[0] = armaCreateMxMatrix(M, 1, mxDOUBLE_CLASS,mxREAL);
    armaSetPr(plhs[0],z);
    plhs[1] = mxCreateDoubleScalar(count_bd);
}