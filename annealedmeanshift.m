function m_mode = annealedmeanshift(m0,m_samples,niter)

% annealed mean shift algorithm to find the mode of posterior distribution
% based on Gibbs samples
% Yu Hang, 2019, NTU
% INPUTS:
%           m0: initial guess of the mode
%           m_samples: P x N matrx Gibbs samples; P is the number of model
%           parameters and N is the sample size
%           niter: number of iterations for annealed mean shift. Large
%           number of iterations lead to more accurate estimates.

% m_sort = sort(m_samples,2);
% diff = m_sort(:,2:end)-m_sort(:,1:end-1);
sub_samples = m_samples(:,1:10:end);
diffmin = norm(sub_samples(:,1)-sub_samples(:,2));
nsamples = size(sub_samples,2);
for i = 1:nsamples-1
    for j = i+1:nsamples
        diff = norm(sub_samples(:,i)-sub_samples(:,j));
        if diff <diffmin
            diffmin = diff;
        end
    end
end
sig_lb = diffmin; %1e-4; %max(diff(:));
sig_ub = 100*sig_lb; %10; %max(diff(:))*100;

ratio = (sig_lb/sig_ub)^(1/(niter-1));

for i = 1:niter
    sig = sig_ub*ratio^(i-1);
    if i == 1
        m_mode = meanshift(m0,m_samples,sig);
    else
        m_mode = meanshift(m_mode,m_samples,0.1);
    end
end