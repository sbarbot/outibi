function [z, count_bd] = truncMVN_fullrank(z, mu_z, A_tilde, a, M, randn_vec, randu_vec)

count_bd = 0;
for i = 1:M
    ic = setdiff(1:M,i);
    idp = find(A_tilde(:,i)<0);
    %             bounds = (a-A(:,ic)*m(ic))./abs(A(:,i));
    if ~isempty(idp)
        upper_bound = min((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        upper_bound = Inf;
    end
    idp = find(A_tilde(:,i)>0);
    if ~isempty(idp)
        lower_bound = max((a(idp)-A_tilde(idp,ic)*z(ic))./A_tilde(idp,i));
    else
        lower_bound = -Inf;
    end
    if lower_bound - upper_bound > 1e-3
       fprintf('lower bound - upper bound = %f\n', lower_bound - upper_bound);
       error('Error: the inequality constraints do not define a feasible set.');
    end
    cdf_upper_bound = normcdf(upper_bound - mu_z(i));
    cdf_lower_bound = normcdf(lower_bound - mu_z(i));
    if cdf_lower_bound > 1 - 1e-8 || cdf_upper_bound < 1e-8
        if upper_bound == Inf %isempty(upper_bound)
            z(i) = lower_bound + abs(randn_vec(i)); 
        elseif lower_bound == -Inf %isempty(lower_bound)
            z(i) = upper_bound - abs(randn_vec(i)); 
        else
            z(i) = (upper_bound-lower_bound)*randu_vec(i)+lower_bound;
        end
    else
        z(i) = norminv((cdf_upper_bound-cdf_lower_bound)*randu_vec(i)+cdf_lower_bound) + mu_z(i);
    end
    if cdf_upper_bound > 1 - 1e-8 && cdf_lower_bound < 1e-8
        count_bd = count_bd + 1;
    end
end