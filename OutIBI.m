function [m_samples,lambda_samples,delta_samples,gamma_samples]= ...
    OutIBI(G,W,d,K,k,lambda0,A,a,q,options)


% Gibbs sampling algorithm for Bayesian inference of linear
% inverse problem with outliers.
%
% Finds m that minimizes lambda1 
%
%        lambda(1)(G{1} m-d{1}-delta{1})^T W{1}(G{1} m-d{1}-delta{1}) + ... 
%        + lambda(m) (G{m} m-d{m}-delta{m})^T W{m}(G{m} m-d{m}-delta{m})
%
% with sparse outliers delta and unknown weights lambda, subject to 
% the following constraints:
%
%                lambda(m+1) (K{1} m - k{1}) = 0
%                    ...
%                lambda(m+n) (K{n} m - k{n}) = 0
%
% with unknown lambda1, ..., lambdan, and the positivity constraints:
%
%                A m >= a
%
% Returns a random population of models m, weights lambda, and outliers
% delta
%
% INPUTS:
% G:         cell array of N{i} x M design matrix with d{i}=G{i}*m
% W:         cell array of N{i} x N{i} inverse covariance matrix
% K:         cell array of equality constraint matrix
% k:         cell array of equality constraint vector
% lambda0:   vector of initial values of lambda
% A:         inequality constraint matrix
% a:         inequality constraint vector
% q:         number of Gibbs samples drawn from the posterior distribution
%            (typcially 1e5, including the burn-in process)
% options:  
%            options.fixed_weights: vector with the same length of
%            lambda0
%            options.fixed_weights(i) = 1: lambda(i) is fixed to be
%            lambda0(i) during the Gibbs sampling process.
%            options.fixed_weights(i) = 0: lambda(i) will be updated
%            during the Gibbs sampling process.
%            options.coupled_prior = 1: use coupled priors as in the paper
%            Moore et al., Imaging the distribution of transient viscosity
%            after the 2016 mw 7.1 kumamoto earthquake, 2017
%            Recommend when the number observed data points is much smaller
%            than the number of model parameters
%            options.coupled_prior = 0: use decoupled priors as in the paper
%            Yu et al., Outlier-Insensitive Bayesian Inferencefor Linear
%            Inverse Problems (OutIBI) with Applications to Space Geodetic
%            Data, 2019. 
%            Recommended when the number observed data points is relatively
%            large. Decoupled priors typically lead to faster convergence 
%            than coupled priors, but may be inaccurate when the number of
%            data points is much smaller than the number of model
%            parameters.
%            options.save_partial_results = 1 (by default): save the partial
%            results every 5000 iterations
% OUTPUS:
% m_samples:         M x q matrix of Gibbs samples of the model parameters m
% lambda_samples:    length(G)+length(K) x q matrix of Gibbs samples of the
%                   weights (i.e., inverse variance)
% delta_samples:     cell array of N{i} x q matrices of Gibbs samples of the
%                   outliers
% gamma_samples:     cell array of N{i} x q matrices of Gibbs samples of the
%                   inverse variance of the outliers
% AUTHOR: Hang Yu, 2019, NTU.


if ~exist('options','var')
      options.fixed_weights = zeros(length(lambda0),1);
      options.coupled_prior = 0;
      options.save_partial_results = 1;
else
    if ~isfield(options, 'fixed_weights')
        options.fixed_weights = zeros(length(lambda0),1);
    else
        assert(length(options.fixed_weights)==length(lambda0),'OutIBI: options.fixed_weights and lambda0 vector are not compatible.');
    end
    if ~isfield(options, 'coupled_prior')
        options.coupled_prior = 0;
    end
    if ~isfield(options, 'save_partial_results')
        options.save_partial_results = 0;
    end
end

idK = find(options.fixed_weights(length(G)+1:end) == 0);
if size(idK,1) ~= 1
    idK = idK';
end

assert(length(G)==length(d),'OutIBI: G matrix and d vector are not compatible.');

if ~iscell(G)
    G={G};
end

if ~iscell(d)
    d={d};
end

if ~iscell(W)
    W={W};
end

if ~iscell(K)
    K={K};
end

if ~iscell(k)
    k={k};
end


assert(length(G)==length(d),'OutIBI: lists of matrix G and vector d must have the same length.');
assert(length(W)==length(d),'OutIBI: lists of matrix W and vector d must have the same length.');
assert(length(K)==length(k),'OutIBI: lists of matrix K and vector k must have the same length.');

for i=1:length(G)
    assert(size(G{i},1)==length(d{i}),sprintf('OutIBI: design matrix and data vector %d are not compatible.\n',i));
end

for i=1:length(K)
    assert(size(K{i},1)==length(k{i}),sprintf('OutIBI: equality constraint matrix and vector %d are not compatible.\n',i));
end

assert(size(A,1)==length(a),sprintf('OutIBI: inequality constraint matrix and vector are not compatible.\n'));


% initilization
if ~isempty(G)
    M = size(G{1},2);
else
    M = size(K{1},2);
end
m_samples = zeros(M,q);
GWG = cell(length(G),1);
lambda = lambda0;  % [1;1e6;45]; % %0.1*
lambda_thr = zeros(length(G),1);
idd = cell(1,length(G));
rank_thr = 0;
for i = 1:length(G)
    GWG{i} = G{i}.'*W{i}*G{i};
    q25 = quantile(d{i},0.25);
    q75 = quantile(d{i},0.75);
    idd{i} = find(d{i}>q25 & d{i}<q75);
    if ~options.fixed_weights(i)
        lambda_thr(i) = 1/var(d{i}(idd{i}));
        lambda(i) = max(lambda_thr(i),1);
    end
    rank_thr = rank_thr + rank(G{i});
end

KK = cell(length(K),1);
Kk = cell(length(K),1);
rank_prior = 0;
for i = 1:length(K)
    KK{i} = K{i}.'*K{i};
%     if size(K{i},1) < M
%         [V,D] = eigs(KK{i},size(K{i},1),'la');
%         KK{i} = V*D*V';
%     end
    Kk{i} = sparse(K{i}.'*k{i});
    rank_prior = rank_prior + rank(full(K{i}));
end

if options.coupled_prior && rank_prior < M
    error(['Error: the total number of equality constraints is smaller than', ...
        'the number of model parameters. Right now, the coupled prior cannot', ...
        'deal with this case. Please provide more equality constraints or use', ...
        'the decoupled prior instead.']);
end

rank_thr = rank_thr + rank_prior;
if rank_thr < M
    warning(['Warning: The posterior distribution of the model parameters is rank-deficient.', ...
    'Please consider adding more data or equality constraints.'])
end

% KK{1} = KK{1}+2e-16*speye(M);

J = 0;
h = 0;
for i = 1:length(G)
    J = J+lambda(i)*GWG{i}; %G{i}(idd{i},:).'*W{i}(idd{i},idd{i})*G{i}(idd{i},:);
    h = h+lambda(i)*(G{i}.'*(W{i}*d{i})); %(G{i}(idd{i},:).'*(W{i}(idd{i},idd{i})*d{i}(idd{i})));
end
for i = 1:length(K)
    J = J+lambda(length(G)+i)*KK{i};
    h = h+lambda(length(G)+i)*Kk{i};
end


if ~isempty(A)
    [~,non_psd] = chol(J);
    if non_psd ~=0
        m = quadprog(J+1e-2*speye(M),-h,-A,-a-5e-1);
    else
        m = quadprog(J,-h,-A,-a-5e-1);
    end
    
%     idA = find(sum(A~=0)>0);
%     m(idA) = A(:,idA)\(a+1e-2);
%     
else
    [~,non_psd] = chol(J);
    if non_psd ~=0
        m = (0.1*speye(M)+J)\h;
    else
        m = J\h;
    end
end

delta = cell(1,length(G));
gamma = cell(1,length(G));
for i = 1:length(G)
   delta{i} = 0; %d{i} - G{i}*m;
   gamma{i} = 0*ones(length(d{i}),1);  % 
end


if options.coupled_prior && ~isempty(K)
    stdK = ones(1,length(K));
    kappa_count = 1e-2*ones(1,length(K));
    
    Jprior = 0;
    for i = 1:length(K)
        Jprior = Jprior+lambda(length(G)+i)*KK{i};
%         h = h+lambda(length(G)+i)*Kk{i};
    end
    logdetJprior0 = 2*sum(log(diag(chol(Jprior)))); %logdet(Jprior);
end

count_bd = 0;

rng('default')


% MCMC algorithm

for kappa = 1:q
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    %
    %              S A M P L I N G   F R O M   p(m|d,delta,lambda)
    %
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    J = 0;
    h = 0;
%     lambdamax = max(lambda)/1e4;
%     lambdatmp = lambda/lambdamax;
    for i = 1:length(G)
        J = J+lambda(i)*GWG{i};
        h = h+lambda(i)*(G{i}.'*(W{i}*(d{i}-delta{i})));
    end
    for i = 1:length(K)
        J = J+lambda(length(G)+i)*KK{i};
        h = h+lambda(length(G)+i)*Kk{i};
    end
    
    [U,non_psd] = chol(J);
    if non_psd ==0 && rank_thr >= M
        mu_z = U'\h;
        
        if isempty(A)  
            m = U\(randn(M,1)+mu_z);
        elseif count_bd == M
            m = U\(randn(M,1)+mu_z);
            count = 0;
            while any(A * m < a)
                m = U\(randn(M,1)+mu_z);
                count = count + 1;
                if count == 10
                    count_bd = 0;
                end
            end
        end
        
        if ~isempty(A) && count_bd < M
            z = U*m;
            A_tilde = full(A/U);
            randn_vec = randn(M,1);
            randu_vec = rand(M,1);
            
            [z, count_bd] = truncMVN_fullrank(z, mu_z, A_tilde, a, M, randn_vec, randu_vec);
            
            m = U\z;
        end    
    else
        if isempty(A)
            error('Error: The inverse covariance matrix J is rank deficient or close to singular,' + ...
                'please provide more data or equality or inequality constraints!')
            vcond = 1./diag(J);
            for i = 1:M
                cid = setdiff(1:M,i);
                m(i) = sqrt(vcond(i))*randn(1)+(h(i)-m(cid)'*J(cid,i))*vcond(i);
            end
        else
            rank_final = min(M - non_psd, rank_thr);
            [V,J_z] = eig(J);
            J_z = diag(J_z);
            J_sort = sort(J_z,'descend');
            J_thr = J_sort(rank_final+1);
            i = 1;
            while J_thr < 0
                J_thr = J_sort(rank_final - i);
                i = i + 1;
            end
            id_nonzero = find(J_z > J_thr)';
            h_z = V'*h;
            std_z = zeros(M,1);
            std_z(id_nonzero) = sqrt(1./J_z(id_nonzero));
            mu_z = zeros(M,1);
            mu_z(id_nonzero) = h_z(id_nonzero)./J_z(id_nonzero);

            z = V'*m;
            A_tilde = full(A*V);
            randn_vec = randn(length(id_nonzero),1);
            randu_vec = rand(M,1);
            
            
            z = truncMVN_rankdeficient(z, mu_z, std_z, id_nonzero, ...
                setdiff(1:M, id_nonzero), A_tilde, a, M, randn_vec, randu_vec);
            
            m = V*z;
        end
    end
    
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    %
    %              S A M P L I N G   F R O M   p(delta|m,gamma,lambda)
    %              & p(gamma|delta) & p(lambda|m,d,delta)
    %
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    for i = 1:length(G)
        J = lambda(i)*W{i}+spdiags(gamma{i},0,length(d{i}),length(d{i}));
        d_Gm = d{i}-G{i}*m;
        h = lambda(i)*(W{i}*d_Gm);
        L = sqrt(diag(J)); %chol(J,'lower');
        delta{i} = (randn(length(d{i}),1)+h./L)./L;%L.'\(randn(length(d{i}),1)+L\h);
        d_Gm_delta = d_Gm-delta{i};
        if kappa < min(q/10, 1e4)
            gamma{i} = gamrnd(0.5+1e-2,1./(delta{i}.^2/2+1e-2));
            if ~options.fixed_weights(i)
                lambda(i) = max(lambda_thr(i),gamrnd(length(d{i})/2+1e-2,1/(d_Gm_delta.'*W{i}*d_Gm_delta/2+1e-2)));
            end
        else
            gamma{i} = gamrnd(0.5+1e-300,1./(delta{i}.^2/2+1e-300)); 
            if ~options.fixed_weights(i)
                lambda(i) = gamrnd(length(d{i})/2+1e-6,1/(d_Gm_delta.'*W{i}*d_Gm_delta/2+1e-6));
            end
        end
    end
    
    
    if options.coupled_prior
    
        rho = 1/(10+kappa^0.3);
        
        for i = idK %1:length(K)
            log_lambdai = randn(1)*stdK(i)+log(lambda(length(G)+i));
            Jpriorh = Jprior + (exp(log_lambdai)- lambda(length(G)+i))*KK{i};
            [U,non_psd] = chol(Jpriorh);
            if non_psd == 0 
                logdetJpriorh = 2*sum(log(diag(U))); %logdet(Jpriorh);
            end
            if log_lambdai >= -100 && non_psd == 0 && rand(1) <= exp((logdetJpriorh - logdetJprior0 - ...
                    (exp(log_lambdai)- lambda(length(G)+i))*sum((k{i}-K{i}*m).^2))/2)
                lambda(length(G)+i) = exp(log_lambdai);
                Jprior = Jpriorh;
                logdetJprior0 = logdetJpriorh;
                kappa_count(i) = kappa_count(i) + 1;
            end
            stdK(i) = exp(log(stdK(i))+rho*(1.4524+log(kappa_count(i)/kappa)));
        end
    else
        for i = idK %1:length(K)
            if kappa <= min(q/10, 1e4) %%
                lambda(length(G)+i) = gamrnd(length(k{i})/2+1e-2,1/(sum((k{i}-K{i}*m).^2)/2+1e-2));
            else
                lambda(length(G)+i) = gamrnd(length(k{i})/2+1e-6,1/(sum((k{i}-K{i}*m).^2)/2+1e-6)); %gamrnd(length(k{i})/2,2/sum((k{i}-K{i}*m).^2));
            end
        end
    end
    
    % output samples
    m_samples(:,kappa) = m;
    if nargout > 1
        if kappa == 1
            lambda_samples = zeros(length(G)+length(K),q);
        end
        lambda_samples(:,kappa) = lambda;
    end
    if nargout > 2
        if kappa == 1
            delta_samples = cell(length(G),1);
            for i = 1:length(G)
                delta_samples{i} = zeros(length(d{i}),q);
            end
        end
        for i = 1:length(G)
            delta_samples{i}(:,kappa) = delta{i};
        end
    end
    if nargout > 3
        if kappa == 1
            gamma_samples = cell(length(G),1);
            for i = 1:length(G)
                gamma_samples{i} = zeros(length(d{i}),q);
            end
        end
        for i = 1:length(G)
            gamma_samples{i}(:,kappa) = gamma{i};
        end
    end
    
    % save partial results
    if options.save_partial_results
        save OutIBI_partial_results m_samples delta_samples gamma_samples lambda_samples -v7.3;
    end
end

figure; plot((cumsum(m_samples,2)./repmat(1:q,M,1))');
xlabel('# iterations');
ylabel('running mean of model parameters');
title('please use the samples after the running means become stable');